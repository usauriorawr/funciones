# Curso JavaScript Coderhouse

## Clase 04 - Programación con funciones

### Simulador interactivo

---

**Tipo de desafío:** 
Entregable.

**Formato:** 
Página HTML y código fuente en JavaScript. Debe identificar el apellido del alumno/a en el nombre de archivo comprimido por "claseApellido".

**Sugerencia:** 
Algunos criterios a tener en cuenta para seleccionar un proceso a simular por primera vez son:

- **ELEGIR UN PROCESO BIEN CONOCIDO:** 
Si conozco una situación que implique adquirir cierta información y estoy bien familizarizado en "cómo se hace", es más fácil traducir la solución a un lenguaje de programación.

- **ELEGIR UN PROCESO QUE ME RESULTE INTERESANTE:** 
Si me siento motivado sobre el tema, es más llevadero enfrentar los retos de desarrollo e interpretación.
Antes de programar existe la etapa de relevamiento y análisis que me permite identificar cómo solucionar el proceso.

---

**>> Consigna:** 
Con los conocimientos vistos hasta el momento, empezarás a armar la estructura inicial de tu proyecto integrador. A partir de los ejemplos mostrados la primera clase, deberás:

- Pensar el alcance de tu proyecto: ¿usarás un cotizador de seguros? ¿un simulador de créditos? ¿un simulador personalizado?

- Armar la estructura HTML del proyecto.

- **Incorporar lo ejercitado en las clases anteriores, algoritmo condicional y algoritmo con ciclo.**

- Utilizar funciones para realizar esas operaciones.


**>> Aspectos a incluir en el entregable:**
Archivo HTML y Archivo JS, referenciado en el HTML por etiqueta `<script src="js/miarchivo.js"></script>`, que incluya la definición de un algoritmo en JavaScript que emplee funciones para resolver el procesamiento principal del simulador.

---

**>> Ejemplos:**
- Calcular costo total de productos y/o servicios seleccionados por el usuario.
- Calcular pagos en cuotas sobre un monto determinado.
- Calcular valor final de un producto seleccionado en función de impuestos y descuentos.
- Calcular tiempo de espera promedio en relación a la cantidad de turnos registrados.
- Calcular edad promedio de personas registradas.
- Calcular nota final de alumnos ingresados.

---

### Descripción de la tarea desarrollada

El programa consiste en procesar un pedido de empanadas de un usuario, solicitándole que ingrese la cantidad y sabores.

También posee descuentos por llevar distintas cantidades y descuentos adicionales si se paga con algunas de las opciones de tarjeta de crédito disponibles.
